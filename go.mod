module github.com/aptly-dev/aptly

go 1.15

require (
	github.com/AlekSi/pointer v1.2.0
	github.com/Azure/azure-storage-blob-go v0.14.0
	github.com/Azure/go-ntlmssp v0.0.0-20211209120228-48547f28849e // indirect
	github.com/DisposaBoy/JsonConfigReader v0.0.0-20201129172854-99cf318d67e7
	github.com/awalterschulze/gographviz v2.0.3+incompatible
	github.com/aws/aws-sdk-go v1.43.11
	github.com/cavaliergopher/grab/v3 v3.0.1
	github.com/cheggaaa/pb v1.0.29
	github.com/gin-contrib/sessions v0.0.4
	github.com/gin-gonic/gin v1.7.7
	github.com/go-asn1-ber/asn1-ber v1.5.3 // indirect
	github.com/go-ldap/ldap/v3 v3.4.2
	github.com/go-playground/validator/v10 v10.10.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/h2non/filetype v1.1.3
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/jlaffaye/ftp v0.0.0-20220301181425-a81b090061fa // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/kjk/lzma v0.0.0-20161016003348-3fd93898850d
	github.com/mattn/go-ieproxy v0.0.3 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mattn/go-shellwords v1.0.12
	github.com/mkrautz/goar v0.0.0-20150919110319-282caa8bd9da
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/mxk/go-flowrate v0.0.0-20140419014527-cca7078d478f
	github.com/ncw/swift v1.0.53
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/pborman/uuid v1.2.1
	github.com/pkg/errors v0.9.1
	github.com/smartystreets/gunit v1.0.4 // indirect
	github.com/smira/commander v0.0.0-20140515201010-f408b00e68d5
	github.com/smira/flag v0.0.0-20170926215700-695ea5e84e76
	github.com/smira/go-aws-auth v0.0.0-20180731211914-8b73995fd8d1
	github.com/smira/go-ftp-protocol v0.0.0-20140829150050-066b75c2b70d
	github.com/smira/go-xz v0.0.0-20201019130106-9921ed7a9935
	github.com/syndtr/goleveldb v1.0.1-0.20190923125748-758128399b1d
	github.com/ugorji/go/codec v1.2.7
	github.com/wsxiaoys/terminal v0.0.0-20160513160801-0940f3fc43a0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9
	golang.org/x/time v0.0.0-20220224211638-0e9765cccd65
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
